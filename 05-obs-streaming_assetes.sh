#!/bin/bash
set -e

#####################
## OBS és kellékei ##
#####################

## QuickSync felvételhez
sudo pacman -S intel-media-sdk --noconfirm --needed
sudo pacman -S intel-media-driver --noconfirm --needed


## Streamdeck és OBS
paru -S --noconfirm --needed streamdeck-ui
paru -S --noconfirm --needed obs-cmd
paru -S --noconfirm --needed obs-studio-tytan652
