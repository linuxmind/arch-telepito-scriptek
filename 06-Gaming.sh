#!/bin/bash
set -e

#####################
## Gaming dolgok   ##
#####################

sudo pacman -S --noconfirm --needed mangohud
sudo pacman -S --noconfirm --needed lib32-mangohud
sudo pacman -S --noconfirm --needed lutris
sudo pacman -S --noconfirm --needed prismlauncher
sudo pacman -S --noconfirm --needed gamemode
sudo pacman -S --noconfirm --needed goverlay
sudo pacman -S --noconfirm --needed mangohud
sudo pacman -S --noconfirm --needed lib32-libvdpau
sudo pacman -S --noconfirm --needed lib32-libva
sudo pacman -S --noconfirm --needed lib32-libxtst
sudo pacman -S --noconfirm --needed lib32-libxrandr
sudo pacman -S --noconfirm --needed lib32-libpulse
sudo pacman -S --noconfirm --needed lib32-gdk-pixbuf2
sudo pacman -S --noconfirm --needed lib32-gtk2
sudo pacman -S --noconfirm --needed lib32-openal
sudo pacman -S --noconfirm --needed steam-native-runtime

