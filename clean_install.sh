#!/usr/bin/env bash

# Editing pacman.conf

  sudo sed -i 's/#Color/Color/' /etc/pacman.conf
  sudo sed -i '38 i ILoveCandy' /etc/pacman.conf

# INSTALLING ARCHLINUX with GNOME

# SZÖVEG
bold=$(tput bold)      # ${bold}
normal=$(tput sgr0)    # ${normal}
yellow=$(tput setaf 3) # ${yellow}

# VÁLTOZÓK
CPU=$(lscpu | grep "AMD" -c)
nVidia=$(lspci -P | grep "NVIDIA" -c)
Locale=$(locale | grep "hu_HU" -c)

# Deleting password for the script to run uninterrapted

    echo "$USER ALL=(ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/99-$USER
    echo "Defaults timestamp_timeout=-1" | sudo tee -a /etc/sudoers

# Making Makepkg a bit faster

    sudo sed -i 's/OPTIONS=(strip docs !libtool !staticlibs emptydirs zipman purge !debug lto !autodeps)/OPTIONS=(strip docs !libtool !staticlibs emptydirs zipman purge !debug !lto !autodeps)/' /etc/makepkg.conf

# Arch Linux linux-headers check and install script
# AlexC (c) 2025

# Get the current kernel version
kernel_version=$(uname -r)

# Check if kernel is hardened, lts, or zen and set the corresponding header package
if [[ "$kernel_version" == *"hardened"* ]]; then
    kernel_header_package="linux-hardened-headers"
    echo "Detected Hardened kernel."
elif [[ "$kernel_version" == *"lts"* ]]; then
    kernel_header_package="linux-lts-headers"
    echo "Detected LTS kernel."
elif [[ "$kernel_version" == *"zen"* ]]; then
    kernel_header_package="linux-zen-headers"
    echo "Detected Zen kernel."
else
    kernel_header_package="linux-headers"  # Default generic headers for Arch
    echo "Detected standard kernel."
fi

# Check the distribution
if [ -f /etc/os-release ]; then
    . /etc/os-release
    distro=$ID
else
    echo "Cannot determine distribution."
    exit 1
fi

# Install the headers based on the detected kernel
if [[ "$distro" == "arch" ]]; then
    echo "Setting up headers for Arch Linux..."

    # Update the package database
    sudo pacman -Sy

    # Install the corresponding headers
    sudo pacman -S --noconfirm "$kernel_header_package"

    echo "Headers installed for kernel version $kernel_version."

else
    echo "Unsupported distribution: $distro"
    exit 1
fi

# Installing packages

    sudo pacman -S gnome-console gnome-session gdm gnome-disk-utility gnome-system-monitor fuse2 eog network-manager-applet networkmanager base-devel bash-completion gedit gnome-calculator baobab ffmpeg git gnome-control-center gparted gnome-tweaks gnome-browser-connector xdg-desktop-portal xdg-desktop-portal-gnome xdg-desktop-portal-gtk xdg-desktop-portal-wlr ttf-ubuntu-font-family gnome-settings-daemon unzip wget glib2 glib2-devel hblock noto-fonts-emoji noto-fonts noto-fonts-extra nano qt5-wayland meson rsync zenity mesa-utils gvfs gvfs-mtp gvfs-smb android-tools wmctrl python-setuptools ufw gufw rebuild-detector --noconfirm
    sudo systemctl enable gdm.service
    sudo systemctl enable NetworkManager.service
    sudo systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target geoclue.service
    sudo sed -i 's/Icon=org.gnome.Console/Icon=org.gnome.Terminal/' /usr/share/applications/org.gnome.Console.desktop
    sudo sed -i 's/Name[en_GB]=Console/Name[en_GB]=Terminal/' /usr/share/applications/org.gnome.Console.desktop
    flatpak remote-add --if-not-exists --user flathub https://dl.flathub.org/repo/flathub.flatpakrepo
    hblock
	
# Flatseal - Flatpak app jogosultság beállító
  echo
  echo ---------------------------------------------
  echo Installing ${bold}${yellow}Flatseal${normal}

   yay -S flatseal --noconfirm
   sudo cp -a overrides /home/$USER/.local/share/flatpak
  echo ${bold}${yellow}Flatseal ${normal}installed.
  echo ---------------------------------------------
  echo

# UCode installer - Credit Lordify
  if [[ $CPU -gt 0 ]]; then
	  sudo pacman -S amd-ucode --noconfirm
  else
	  sudo pacman -S intel-ucode --noconfirm
  fi

# Adding ucode to boot entries - Credit Lordify
  if [[ $CPU -gt 0 ]]; then
	  echo "initrd   /amd-ucode.img" | sudo tee -a /boot/loader/entries/*.conf
  else
	  echo "initrd   /intel-ucode.img" | sudo tee -a /boot/loader/entries/*.conf
  fi

# nVidia - Credit to Lordify
if [[ $nVidia -gt 0 ]]; then
	sudo pacman -S nvidia-dkms nvidia-utils nvidia-settings egl-wayland lib32-nvidia-utils --noconfirm # nVidia-DKMS and some Wayland Packages
	sleep 1
	sudo sed -i 's/MODULES=()/MODULES=(nvidia nvidia_modeset nvidia_uvm nvidia_drm)/' /etc/mkinitcpio.conf # Add early loading of nVidia kernel mode setting
	sleep 1
	echo "options nvidia_drm modeset=1 fbdev=1" | sudo tee /etc/modprobe.d/nvidia.conf > /dev/null # Make sure it is loaded...
	sleep 1
	sudo mkinitcpio -P
fi
# Cleanup
    sudo rm /etc/sudoers.d/99-$USER
    sudo sed -i '140d' /etc/sudoers
    sudo rm -rf /home/$USER/cosmetics

# End of script
  echo Reboot in 3s...
  echo Reboot in 2s...
  echo Reboot in 1s...
  echo Reboot
  reboot